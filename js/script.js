window.onload = init;

    	function init() {

        /*Pseudo jquery wrappers*/

        /*Object function used to pass in element ID
        *@return object ID*/
        function O(obj) {
        	if (typeof obj == 'object') return obj
        		else return document.getElementById(obj)
        	}

        /*Style function  
        * @return style property of the passed in element ID*/
        function S(obj) {
        	return O(obj).style
        }

        /*Class function
        *@return an array where all matching objects are placed*/
        function C(name) {
        	var elements = document.getElementsByTagName('*')
        	var objects = []

        	for (var i = 0; i < elements.length; i++)
        		if (elements[i].className == name)
        			objects.push(elements[i])

        		return objects

        	}

        	var range = O('range');
        	var goal = O('goal');
        	var sport = O('sport');
        	var articles = O('articles');
        	var clothing = O('clothing');
        	var menu_regions = [range, goal, sport, articles, clothing];
        	var header = O('section-nav'),
        	header_logo = O('section-nav-logo'),
        	header_height = getComputedStyle(header).height.split('px')[0],
        	account_label = O('account-label'),
        	account_tab = O('account'),
        	search_form = document.getElementsByClassName('search-wrap-fixed')[0],
        	menu_wrap = document.getElementsByClassName('nav-menu')[0],
        	fix_class = ['section-nav-fixed', 'section-nav-logo-fixed', 'account-label-hide', 'slide-in-left'];
     

        var menu_item = O('menu-icon');
        var login_dropdown = O('login-dropdown');




        /*Account dropdown display on click*/
        account.onclick = function(e) {
        	e.preventDefault();

            /*If display of element is block, set to none.
            * Else if element is display: none, display element*/
            S(login_dropdown).display = (S(login_dropdown).display == "block") ? "none" : "block";
        }


        /*Dropdown menu navigation display on icon click*/
        menu_item.onclick = function(e) {
        	e.preventDefault();

        	/*If menu_regions style is block, then instantly set it to none*/


        	/*Iterate through the parent menu items*/
        	for (var i = 0; i < menu_regions.length; i++) {

        		/*If display is = block then set the display to none*/

        		if (menu_regions[i].style.display !== 'none' || '') {

        			menu_regions[i].style.display = 'block';

        		} else {
        			/*Else set the display to block irrespectively*/

        			menu_regions[i].style.display = 'block';
        		}

        	}
        	/*Set the menu to display as block, then add the slide in class to the menu*/
        	menu_wrap.style.display = "block";
        	menu_wrap.classList.add(fix_class[3]);

        }

        /*Attempt to close the menu when click outside of the element works.
        * But fails when click inside menu on a child element*/
        //     	window.addEventListener('mouseup', function(e){
        //     		var nav_wrap = document.getElementsByClassName('nav-wrap')[0];
        //     		console.log(e.target.parentNode);
        // 	if(e.target != nav_wrap){
        // 		menu_wrap.classList.remove('slide-in-left');

        // 	}
        // });

        /*Range dropdown menu - if block, then expand the drop down menu*/
        range.onclick = function(e) {
        	e.stopPropagation();
        	title_dropdown = range.getElementsByTagName('ul')[0];

        	S(title_dropdown).display = (S(title_dropdown).display == "block") ? "none" : "block";

        	if (title_dropdown.style.display == "block") {
        		document.getElementsByClassName('mobile-parent-expand')[0].classList.add('fa-minus');

        	} else {

        		document.getElementsByClassName('mobile-parent-expand')[0].classList.remove('fa-minus');

        	}

        }

        /*On click of title dropdown, show/hide dropdown and add/remove chevron*/

        O('title-dropdown-trigger').onclick = function(e) {
        	e.stopPropagation();
        	var title_dropdown = document.getElementsByClassName('main-wrap')[0];
        	S(title_dropdown).display = (S(title_dropdown).display == "block") ? "none" : "block";

        	/*Obtain the second child node of the title dropdown (span)*/
        	var node = e.target.childNodes.item(1);

            /*If mainwrap dropdown is displayed, add chevron up to parent
            * Else add the chevron down again*/
            if (title_dropdown.style.display == "block") {

            	node.classList.remove('fa-chevron-down');
            	node.classList.add('fa-chevron-up');


            } else {

            	node.classList.add('fa-chevron-down');

            }
        }

          /*Goal menu item + associated dropdown display/hide*/

        goal.onclick = function(e) {
        	e.stopPropagation();
        	title_dropdown = goal.getElementsByTagName('ul')[0];

        	S(title_dropdown).display = (S(title_dropdown).display == "block") ? "none" : "block";

        	if (title_dropdown.style.display == "block") {
        		document.getElementsByClassName('mobile-parent-expand')[1].classList.add('fa-minus');

        	} else {

        		document.getElementsByClassName('mobile-parent-expand')[1].classList.remove('fa-minus');

        	}


        }

          /*Sport menu item + associated dropdown display/hide*/

        sport.onclick = function(e) {
        	e.stopPropagation();
        	title_dropdown = sport.getElementsByTagName('ul')[0];

        	S(title_dropdown).display = (S(title_dropdown).display == "block") ? "none" : "block";

        	if (title_dropdown.style.display == "block") {
        		document.getElementsByClassName('mobile-parent-expand')[2].classList.add('fa-minus');

        	} else {

        		document.getElementsByClassName('mobile-parent-expand')[2].classList.remove('fa-minus');

        	}

        }

          /*Articles menu item + associated dropdown display/hide*/
        articles.onclick = function(e) {
        	e.stopPropagation();
        	title_dropdown = articles.getElementsByTagName('ul')[0];

        	S(title_dropdown).display = (S(title_dropdown).display == "block") ? "none" : "block";

        	if (title_dropdown.style.display == "block") {
        		document.getElementsByClassName('mobile-parent-expand')[3].classList.add('fa-minus');

        	} else {

        		document.getElementsByClassName('mobile-parent-expand')[3].classList.remove('fa-minus');

        	}

        }

        /*Clothing menu item + associated dropdown display/hide*/
        clothing.onclick = function(e) {
        	e.stopPropagation();
        	title_dropdown = clothing.getElementsByTagName('ul')[0];

        	S(title_dropdown).display = (S(title_dropdown).display == "block") ? "none" : "block";

        	if (title_dropdown.style.display == "block") {
        		document.getElementsByClassName('mobile-parent-expand')[4].classList.add('fa-minus');

        	} else {

        		document.getElementsByClassName('mobile-parent-expand')[4].classList.remove('fa-minus');

        	}

        }

        /*Menu close*/
        O('menu-close').onclick = function(e) {
        	e.preventDefault();
        	/*remove slide left class, then adds slide back class*/
        	menu_wrap.classList.remove(fix_class[3]);

        }


        /*Cycles through all H4 titles in dropdowns and adds event listener to each.
         *Gets the currently selected node and then sets the UL element sibling to 'block' display
         */

         var dropdown_item_title = document.querySelectorAll('.dropdown-item-title');


         for (var i = 0; i < dropdown_item_title.length; i++) {
         	dropdown_item_title[i].addEventListener('click', function(e) {
         		e.stopPropagation();
                // console.log(e.target);
                var node = this;
                /*Get the UL with class 'dropdown item links"*/
                var elem = node.nextElementSibling;
                elem.style.display = (elem.style.display == "block") ? "none" : "block";
                var node = e.target.childNodes.item(1);


                /*If mainwrap dropdown is displayed, add chevron up to parent
                * Else add the chevron down again*/
                if (elem.style.display == "block") {

                	node.classList.remove('fa-chevron-down');
                	node.classList.add('fa-chevron-up');


                } else {

                	node.classList.remove('fa-chevron-up');
                	node.classList.add('fa-chevron-down');

                }
            });

         }






         /*Detect the media query size > laptop media query*/
        // var mq = window.matchMedia( "(min-width:1200px)" );
        //     	if(mq.matches){
        //     		console.log('Media query met!');
        //     	}

        if (matchMedia) {
        	var mq = window.matchMedia("(min-width:1200px)");
        	mq.addListener(WidthChange);
        	WidthChange(mq);

        }

        function WidthChange() {
        	if (mq.matches) {


        		function stickyScroll(e) {
        			if (window.pageYOffset > header_height) {
        				header.classList.add(fix_class[0]);
        				header_logo.id = fix_class[1];
        				account_label.id = fix_class[2];
        				search_form.style.display = 'block';



        			}

        			if (window.pageYOffset < header_height) {
        				header.classList.remove(fix_class[0]);
        				header_logo.id = "section-nav-logo";
        				account_label.id = "account-label";
        				search_form.style.display = 'none';
        			}
        		}

        		window.addEventListener('scroll', stickyScroll, false);
        	} else {

        		header.classList.remove(fix_class[0]);
        	}

        }

    }